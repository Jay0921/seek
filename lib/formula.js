const User = require('../app/Collection/User');
const Product = require('../app/Domain/Product');


var priceList;
  Product
    .getPrice()
    .then((docs)=>{
      priceList = docs;
    })

module.exports = {
  


  calculate(data) {
    let totalPrice = 0;

    User.findById(data.buyer, (err, user)=>{
      data.items.forEach((item, key)=>{
        let pricingRules = user.privilege[item.product];
        if(user.privilege && pricingRules) {

            pricingRules.forEach((rule, index)=>{
              switch (rule.type) {
                case 'discount':
                  totalPrice += this.discount(priceList, rule, data);
                  break;
                case 'free':
                  totalPrice += this.freeDiscount(priceList, rule, data);
                break;
                case 'quantity':
                  totalPrice += this.quantityDiscount(priceList, rule, data);
                break;
              }
            });
    
        } else {
          let originalProduct = _.find(priceList, ['id', item.product]);
          totalPrice += (originalProduct.price * item.quantity);
        }
      });
    });
    return totalPrice = totalPrice.toFixed(2);
  },
}