const express = require('express');
const Product = require('../../../app/Domain/Product');

const router = express.Router();

router.post('/product', (req, res, next) => {
  Product
    .create(req.body)
    .then((doc)=>{
      res.send(doc);
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/product', (req, res, next) => {
  Product
    .getProducts()
    .then((doc)=>{
      res.send(doc);
    })
    .catch((error) => {
      res.send(error);
    });
});

module.exports = router;