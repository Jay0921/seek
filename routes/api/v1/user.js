const express = require('express');
const User = require('../../../app/Domain/User');

const router = express.Router();

router.post('/user', (req, res, next) => {
  User
    .create(req.body)
    .then((doc)=>{
      res.send(doc);
    })
    .catch((error) => {
      res.send(error);
    });
});

router.post('/user/update', (req, res, next) => {
  User
    .updateUser({
      id: req.body.id,
      isPrivilege: req.body.isPrivilege,
      privilege: JSON.parse(req.body.privilege)
    })
    .then((doc)=>{
      res.send('yes');
    })
    .catch((error) => {
      res.send('error');
    });
});

router.get('/user/get/:id', (req, res, next) => {
  const id = req.params.id;
  User.getUser(id)
    .then((doc) => {
      res.render('admin/user', {
        doc
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/user/remove/:id', (req, res, next) => {
  User
    .remove(req.body)
    .then((doc)=>{
      res.send(doc);
    })
    .catch((error) => {
      res.send(error);
    });
});

module.exports = router;