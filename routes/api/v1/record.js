const express = require('express');
const Record = require('../../../app/Domain/Record');

const router = express.Router();

router.post('/record', (req, res, next) => {
  Record
    .create({
      buyer: req.body.buyer,
      items: JSON.parse(req.body.items)
    })
    .then((doc)=>{
      res.send(doc);
    })
    .catch((error) => {
      res.send(error);
    });
});

module.exports = router;