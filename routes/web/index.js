const express = require('express');
const User = require('../../app/Domain/User');
const Product = require('../../app/Domain/Product');

const router = express.Router();


router.get('/', (req, res, next) => {
  User.getUers()
    .then((docs) => {
      res.render('index', {
        docs
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/admin/product', (req, res, next) => {
  res.render('admin/product');
});

router.get('/admin/product-list', (req, res, next) => {
  Product.getProducts()
    .then((docs) => {
      res.render('admin/productList', {
        docs
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/admin/register', (req, res, next) => {
  res.render('admin/register');
});

router.get('/record', (req, res, next) => {
  User.getUers()
    .then((docs) => {
      res.render('admin/record', {
        docs
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/admin/user', (req, res, next) => {
  User.getUers()
    .then((docs) => {
      res.render('admin/userList', {
        docs
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.get('/user/:id', (req, res, next) => {
  const id = req.params.id;
  User.getUser(id)
    .then((doc) => {
      res.render('admin/user', {
        doc
      });
    })
    .catch((error) => {
      res.send(error);
    });
});

module.exports = router;