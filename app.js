require('dotenv').config()

const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const apiV1 = require('./routes/api/v1');
const webRoute = require('./routes/web');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')

app.use('/', express.static(path.join(__dirname, 'public')))

mongoose.connect('mongodb://localhost:27017/seek', { useNewUrlParser: true }, (error) => {
  if (error) {
    console.log(error.name);
  } else {
    console.log('Connected to mongdoDB');
  }
});


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api/v1', apiV1);
app.use('/', webRoute);

app.listen(process.env.PORT, () => {
  console.log(`Connected to Port ${process.env.PORT}`);
});
