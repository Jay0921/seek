const Product = require('../Collection/Product');

module.exports = {
  create(data) {
    return new Promise((resolve, reject) => {
      data.id = data.name.toLowerCase().replace(/\s/g,'-')
      const product = new Product(data);
      product
        .save()
        .then((doc) => {
          resolve(doc)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  getPrice() {
    return new Promise((resolve, reject) => {
      Product
        .find({}, 'id price')
        .exec((err, docs)=>{
          if(docs) {
            resolve(docs)
          } else {
            reject(err);
          }
        });
    });
  },
  getProducts() {
    return new Promise((resolve, reject) => {
      Product
        .find()
        .then((docs) => {
          resolve(docs)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
}