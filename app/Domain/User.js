const User = require('../Collection/User');

module.exports = {

  create(data) {
    return new Promise((resolve, reject) => {
      const user = new User(data);
      user
        .save()
        .then((doc) => {
          resolve(doc)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  create(data) {
    return new Promise((resolve, reject) => {
      const user = new User(data);
      user
        .save()
        .then((doc) => {
          resolve(doc)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  getUser(id) {
    return new Promise((resolve, reject) => {
      User
        .findById(id)
        .then((docs) => {
          resolve(docs)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  getUers() {
    return new Promise((resolve, reject) => {
      User
        .find()
        .then((docs) => {
          resolve(docs)
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  updateUser(data) {
    return new Promise((resolve, reject) => {
      User
        .findByIdAndUpdate(data.id, { $set: { isPrivilege: data.isPrivilege, privilege: data.privilege}}, {new: true})
        .then((docs) => {
          resolve(docs)
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

}