const User = require('../Collection/User');
const Record = require('../Collection/Record');
const Product = require('../Domain/Product');

const _ = require('lodash');
// const Formula = require('../../lib/formula');

var priceList;
  Product
    .getPrice()
    .then((docs)=>{
      priceList = docs;
    })

function discount(priceList, rule, data) {
  return rule.price * data.quantity;
}

function quantityDiscount(priceList, rule, data) {
  if (data.quantity >= rule.quantity) {
    return rule.price * data.quantity;
  }
}

function freeDiscount(priceList, rule, data) {
  let discountQuantity;
  let originalProduct = _.find(priceList, ['id', data.product]);

  if (data.quantity >= rule.quantity) {
    let freeQuantity =  Math.floor(data.quantity / rule.quantity) * rule.free
    let finalQuantity = data.quantity - freeQuantity;
    return (finalQuantity * originalProduct.price)
  } else {
    return (data.quantity * originalProduct.price);
  }
}


module.exports = {
  create(data) {
    let totalPrice = 0;
    return new Promise((resolve, reject) => {

      User.findById(data.buyer, (err, user)=>{
        data.items.forEach((item, key)=>{
          let pricingRules = user.privilege[item.product];
          if(user.privilege && pricingRules) {
  
              pricingRules.forEach((rule, index)=>{
                switch (rule.type) {
                  case 'discount':
                    totalPrice += discount(priceList, rule, item);
                    break;
                  case 'free':
                    totalPrice += freeDiscount(priceList, rule, item);
                  break;
                  case 'quantity':
                    console.log(quantityDiscount(priceList, rule, item));
                    totalPrice += quantityDiscount(priceList, rule, item);
                  break;
                }
              });
            
          } else {
            let originalProduct = _.find(priceList, ['id', item.product]);
            totalPrice += (originalProduct.price * item.quantity);
          }
        });
        console.log(totalPrice);
      }).then(()=>{
        console.log(totalPrice);
        const record = new Record({
          buyer: data.buyer,
          price: totalPrice,
          items: data.items
        });
        record
          .save()
          .then((doc) => {
            resolve(doc)
          })
          .catch((error) => {
            reject(error);
          });
      });

      
    });
  },

  

}