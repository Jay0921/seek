const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recordSchema = new Schema(
  {
    buyer: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    price: {
      type: Number,
      required: true
    },
    items: [
      {
        product: String,
        quantity: Number,
      }
    ],
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('Record', recordSchema);