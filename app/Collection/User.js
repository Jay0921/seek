const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true
    },
    isPrivilege: {
      type: Boolean,
      required: true,
      default: false
    },
    privilege: Schema.Types.Mixed
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('User', userSchema);