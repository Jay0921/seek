const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    id: {
      type: String,
      unique: true
    },
    name: {
      type: String,
    },
    price: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('Product', productSchema);