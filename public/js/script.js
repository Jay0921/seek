function validateForm() {
  var errors = validate($('form'), constraints, {format: "detailed"});
  
  $('.form-group').removeClass('has-error');
  $('.help-block').html('');

  errors.forEach(function(error){
    var $formGroup = $('[name='+error.attribute+']').closest('.form-group');
    $formGroup.addClass('has-error');
    $formGroup.find('.help-block').html(error.error);
  });
  return errors;
}
